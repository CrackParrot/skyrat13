/obj/item/clothing/head/soft/sec/brig_phys
	name = "security medic cap"
	icon = 'modular_skyrat/icons/obj/clothing/hats.dmi'
	mob_overlay_icon = 'modular_skyrat/icons/mob/clothing/head.dmi'
	icon_state = "secmedsoft"
	item_state = "secmedsoft"
	unique_reskin = list(
	)

/obj/item/clothing/head/soft/sec
	unique_reskin_icons = list(
	"Default" = 'icons/obj/clothing/hats.dmi',
	"Alternate" = 'modular_skyrat/icons/obj/clothing/caps.dmi',
	"Corporate" = 'modular_skyrat/icons/obj/clothing/caps.dmi',
	"Sol Federation" = 'modular_skyrat/icons/obj/clothing/caps.dmi',
	"Expedition" = 'modular_skyrat/icons/obj/clothing/caps.dmi',
	"Fleet" = 'modular_skyrat/icons/obj/clothing/caps.dmi'
	)
	unique_reskin_worn = list(
	"Default" = 'icons/mob/clothing/head.dmi',
	"Alternate" = 'modular_skyrat/icons/mob/clothing/cap.dmi',
	"Corporate" = 'modular_skyrat/icons/mob/clothing/cap.dmi',
	"Sol Federation" = 'modular_skyrat/icons/mob/clothing/cap.dmi',
	"Expedition" = 'modular_skyrat/icons/mob/clothing/cap.dmi',
	"Fleet" = 'modular_skyrat/icons/mob/clothing/cap.dmi'
	)
	unique_reskin_worn_muzzled = list(
	"Default" = 'icons/mob/clothing/head_muzzled.dmi',
	"Alternate" = 'modular_skyrat/icons/mob/clothing/cap_muzzled.dmi',
	"Corporate" = 'modular_skyrat/icons/mob/clothing/cap_muzzled.dmi',
	"Sol Federation" = 'modular_skyrat/icons/mob/clothing/cap_muzzled.dmi',
	"Expedition" = 'modular_skyrat/icons/mob/clothing/cap_muzzled.dmi',
	"Fleet" = 'modular_skyrat/icons/mob/clothing/cap_muzzled.dmi'
	)
	unique_reskin = list(
	"Default" = "secsoft",
	"Alternate" = "secsoft",
	"Corporate" = "corpsoft",
	"Sol Federation" = "solsoft",
	"Expedition" = "expeditionsoft",
	"Fleet" = "fleetsoft"
	)